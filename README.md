## iframe-resizer-demo
Resize an iframe from another domain in multiple ways
## 用法
该包的js文件夹中包含两个缩小的 JavaScript 文件。第一个 ( iframeResizer.min.js ) 用于托管 iFrame 的页面。可以用原生JavaScript 调用；
```javascript
const iframes = iFrameResize( [{options}], [css selector] || [iframe] );
```
第二个文件 ( iframeResizer.contentWindow.min.js ) 需要放置在 iFrame 中包含的页面中。该文件被设计为其他人系统上的来宾，因此没有依赖性，并且在被包含页面的消息激活之前不会执行任何操作。
### 典型设置
正常的配置是当浏览器窗口大小改变或者 iFrame 内容改变时，让 iFrame 调整大小。要进行此设置，您需要将 iFrame 的一个维度配置为百分比，并告诉库仅更新另一个维度。通常您会将宽度设置为 100% 并设置高度比例以适应内容。
```html
<style>
  iframe {
    width: 1px;
    min-width: 100%;
  }
</style>
<script src="/js/iframeResizer.min.js"></script>
<iframe id="myIframe" src="http://anotherdomain.com/iframe.html"></iframe>
<script>
  iFrameResize({ log: true }, '#myIframe')
</script>
```
注意：使用min-width设置 iFrame 的宽度可以解决 iOS 中阻止 iFrame 正确调整大小的问题。
## How to use

Copy the repo into your disk.

```bash
$ git clone https://github.com/aaronpan/iframe-resizer-demo.git
```

Then play with the source files under the repo's demo* directories.

## Index

1. [Subdomain iframe resize](#demo01-subdomain-iframe-resize)
1. [Use postMessage](#demo02-use-postmessage)
1. [Web proxy for cross-domain iframe resize](#demo03-web-proxy-for-cross-domain-iframe-resize)
1. [Use iframe-resizer library](#demo04-use-iframe-resizer-library)

---

## Demo01: Subdomain iframe resize

a.html in domain sub1.aaron.com.

b.html in domain sub2.aaron.com.

a embed b

This demo only works for subdomain cross-iframe.

## Demo02: Use postMessage

a.html in domain domain1.com.

b.html in domain domain2.com.

a embed b

Use HTML5 postMessage.

Support: Internet Explorer 8, Firefox 3, Opera 9, Chrome 3和 Safari 4.

## Demo03: Web proxy for cross-domain iframe resize

a.html and c.html in domain domain1.com.

b.html in domain domain2.com.

a embed b
b embed c

## Demo04: Use iframe-resizer library

a.html in domain domain1.com.

b.html in domain domain2.com.

a embed b

Dependency free, IE8+


## Related Projects

- [jquery](https://github.com/jquery/jquery/tree/1.12-stable)

- [iframe-resizer](https://github.com/davidjbradshaw/iframe-resizer)

## License

MIT